package asia.bluebelt.bbbackstack;

import android.os.Bundle;

/**
 * Created by Bluebee on 3/20/2018.
 */

public interface FragmentDisplay {
    void showFragment(int tag,Bundle bundle);
    void updateTitle(String newTitle);
}

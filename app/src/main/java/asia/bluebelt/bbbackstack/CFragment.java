package asia.bluebelt.bbbackstack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import static asia.bluebelt.bbbackstack.Constant.D_TAG;
import static asia.bluebelt.bbbackstack.Constant.E_TAG;

/**
 * Created by Bluebee on 3/20/2018.
 */

public class CFragment extends BaseFragment {
    private View view ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment,container,false);
            view.findViewById(R.id.click).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDisplay().showFragment(E_TAG,null);
                }
            });
        }
        return view;
    }

    @Override
    public void onVisible() {
        getDisplay().updateTitle("CCCC");
    }

    @Override
    public void onInvisible() {

    }
}

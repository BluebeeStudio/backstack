package asia.bluebelt.bbbackstack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by Bluebee on 3/20/2018.
 */

public abstract class BaseFragment extends  Fragment {
    private FragmentDisplay mFragmentDisplay;


    public FragmentDisplay getDisplay() {
        return mFragmentDisplay;
    }


    public abstract void onVisible();

    public abstract void onInvisible();

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()){
            onVisible();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getUserVisibleHint()){
            onInvisible();
        }
    }
    public void onTabSelected(boolean selected){

        if(isVisible()){
            if(selected){
                onVisible();
            }else{
                onInvisible();
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentDisplay = (FragmentDisplay) getActivity();
    }
}

package asia.bluebelt.bbbackstack;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import static asia.bluebelt.bbbackstack.Constant.C_TAG;
import static asia.bluebelt.bbbackstack.Constant.D_TAG;
import static asia.bluebelt.bbbackstack.Constant.E_TAG;
import static asia.bluebelt.bbbackstack.Constant.MAIN_TAG;

public class MainActivity extends AppCompatActivity implements FragmentDisplay{

    private FragmentManager mFragmentManager;
    private MainFragment mainFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
        mFragmentManager = getSupportFragmentManager();
        showFragment(MAIN_TAG,null);
    }

    private Fragment createNewFragmentForTag(int tag) {
        switch (tag) {
            case MAIN_TAG:
                if (mainFragment == null) {
                    mainFragment = new MainFragment();
                }
                return mainFragment;
            case C_TAG:
                return new CFragment();
            case D_TAG:
                return new DFragment();
            case E_TAG:
                return new EFragment();
        }
        return null;
    }

    private String getTag(int tag){
        return String.valueOf(tag);
    }

    @Override
    public void showFragment(int tag, Bundle bundle) {
        if (mFragmentManager != null) {
            Fragment fragment = mFragmentManager.findFragmentByTag(getTag(tag));
            if (fragment == null) {
                fragment = createNewFragmentForTag(tag);
            }
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            /*transaction.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                    R.animator.slide_out_right, R.animator.slide_in_right);*/
            if (tag != MAIN_TAG) {// this is root fragment
                transaction.addToBackStack(null);
            }
            transaction.setReorderingAllowed(true);
            transaction.replace(R.id.container, fragment, getTag(tag));
            transaction.commit();

        }
    }

    @Override
    public void updateTitle(String newTitle) {
        setTitle(newTitle );
    }

}

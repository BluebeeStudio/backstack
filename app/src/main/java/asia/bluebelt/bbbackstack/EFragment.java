package asia.bluebelt.bbbackstack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import static asia.bluebelt.bbbackstack.Constant.C_TAG;

/**
 * Created by Bluebee on 3/20/2018.
 */

public class EFragment extends BaseFragment {
    private View view ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment,container,false);
        }
        return view;
    }

    @Override
    public void onVisible() {
        getDisplay().updateTitle("EEEE");
    }

    @Override
    public void onInvisible() {

    }


}

package asia.bluebelt.bbbackstack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

/**
 * Created by Bluebee on 3/20/2018.
 */

public class MainFragment extends BaseFragment {
    View view;
    TabLayout tabLayout;
    ViewPager viewPager;
    PagerAdapterMain pagerAdapterMain;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_main, container, false);
            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
            tabLayout.addTab(tabLayout.newTab().setText("A"));
            tabLayout.addTab(tabLayout.newTab().setText("B"));

            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    setCurrentItem(tab.getPosition());
                }
                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }
                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                }
            });
            pagerAdapterMain = new PagerAdapterMain(getChildFragmentManager());// FOCUS
            viewPager.setAdapter(pagerAdapterMain);
            //viewPager.setOffscreenPageLimit(2);
            setCurrentItem(0);

        }
        return view;
    }

    public void setCurrentItem(int position) {
        if (viewPager != null) {
            viewPager.setCurrentItem(position);
        }
        if (pagerAdapterMain != null) {
            ((AFragment) pagerAdapterMain.getItem(0)).onTabSelected(position == 0);
            ((BFragment) pagerAdapterMain.getItem(1)).onTabSelected(position == 1);
        }
    }

    @Override
    public void onVisible() {

    }

    @Override
    public void onInvisible() {

    }


    private class PagerAdapterMain extends FragmentPagerAdapter {
        AFragment aFragment;
        BFragment bFragment;
        public PagerAdapterMain(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    if(aFragment == null){
                        aFragment = new AFragment();
                    }
                    return aFragment;
                case 1:
                    if(bFragment == null){
                        bFragment = new BFragment();
                    }
                    return bFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
